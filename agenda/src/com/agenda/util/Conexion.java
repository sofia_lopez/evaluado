package com.agenda.util;

import java.sql.Connection;
import java.sql.DriverManager;

public class Conexion {

    private String user = "root";
    private String pass = "root";
    private String url = "jdbc:mysql://localhost:3306/agendaElectronica?allowPublicKeyRetrieval=true&useSSL=false";
    private Connection conn;

    public Connection getConn() {
        return conn;
    }

    public void setConn(Connection conn) {
        this.conn = conn;
    }

    public Connection conectar() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, user, pass);
            if (conn != null) {
                System.out.println("Exitos!!!!!");
            }
            return conn;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void desconectar() {
        try {
            conn.close();
        } catch (Exception e) {

        }
    }


}
