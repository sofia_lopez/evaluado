package com.agenda.ctrl;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.agenda.impl.PersonasImpl;
import com.agenda.impl.TipoUsuarioImpl;
import com.agenda.impl.UsuariosImpl;
import com.agenda.model.Personas;
import com.agenda.model.Tipousuario;
import com.agenda.model.Usuarios;

@Component
@SessionScoped
@ManagedBean
public class UsuariosDao implements Serializable {

	@Autowired
	public UsuariosImpl daoUser;

	@Autowired
	public TipoUsuarioImpl daoTipo;

	@Autowired
	public PersonasImpl daoPerson;

	private List<Usuarios> listaUser;
	private Usuarios usuarios;

	private List<Tipousuario> listaTipous;
	private Tipousuario tipousuario;

	private List<Personas> listaPersonas;
	private Personas personas;

	private String usuario;
	private String pass;

	public List<Usuarios> getListaUser() {
		this.listaUser = daoUser.findAll();
		return listaUser;
	}

	public void setListaUser(List<Usuarios> listaUser) {
		this.listaUser = listaUser;
	}

	public Usuarios getUsuarios() {
		return usuarios;
	}

	public void setUsuarios(Usuarios usuarios) {
		this.usuarios = usuarios;
	}

	public List<Tipousuario> getListaTipous() {
		this.listaTipous = daoTipo.findAll();
		return listaTipous;
	}

	public void setListaTipous(List<Tipousuario> listaTipous) {
		this.listaTipous = listaTipous;
	}

	public Tipousuario getTipousuario() {
		return tipousuario;
	}

	public void setTipousuario(Tipousuario tipousuario) {
		this.tipousuario = tipousuario;
	}

	public List<Personas> getListaPersonas() {
		this.listaPersonas = daoPerson.findAll();
		return listaPersonas;
	}

	public void setListaPersonas(List<Personas> listaPersonas) {
		this.listaPersonas = listaPersonas;
	}

	public Personas getPersonas() {
		return personas;
	}

	public void setPersonas(Personas personas) {
		this.personas = personas;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	private Usuarios user2;
	String mensaje = "";

	public String access() {
		FacesContext fc = FacesContext.getCurrentInstance();
		usuarios = daoUser.access(usuario, pass);
		if (usuarios != null) {
			user2 = usuarios;
			fc.getExternalContext().getSessionMap().put("us", user2);
			return "fechasE?faces-redirect=true";
		} else {
			fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Contrase�a y	 Usuario Incorrectos!",
					"No esta registrado, verifique si su contrase�a y su usuario son correctos"));
			return "index?faces-redirect=false";
		}
	}

	@SuppressWarnings("unlikely-arg-type")
	public String close() {
		FacesContext fc = FacesContext.getCurrentInstance();
		user2 = (Usuarios) fc.getExternalContext().getSessionMap().get("us");
		fc.getExternalContext().invalidateSession();
		fc.getExternalContext().getSessionMap().remove(usuarios.getId());
		System.out.println("Verficar: " + usuarios.getId());
		return "index?faces-redirect=true";

	}

	public void verificarSesion() {
		try {
			FacesContext contex = FacesContext.getCurrentInstance();
			user2 = (Usuarios) contex.getExternalContext().getSessionMap().get("us");
			if (user2 == null) {
				contex.getExternalContext().redirect("index.xhtml");
			}
		} catch (Exception e) {
			System.out.println("Error : " + e.getMessage());
			e.printStackTrace();
		}
	}

	@PostConstruct
	public void init() {
		usuarios = new Usuarios();
		tipousuario = new Tipousuario();
		personas = new Personas();
	}

	public void createUs() {
		try {
			usuarios.setPersonas(personas);
			usuarios.setTipoU(tipousuario);
			daoUser.create(usuarios);
			mensaje = "Registrado";
			init();
		} catch (Exception e) {
			mensaje = "Error al guardar";
			e.printStackTrace();
		}
		FacesMessage msj = new FacesMessage(this.mensaje);
		FacesContext.getCurrentInstance().addMessage(null, msj);
	}
	
	public void editUs() {
		try {
			usuarios.setPersonas(personas);
			usuarios.setTipoU(tipousuario);
			daoUser.create(usuarios);
			mensaje = "Editado";
			init();
		}catch(Exception e) {
			e.printStackTrace();
		}
		FacesMessage msj = new FacesMessage(this.mensaje);
		FacesContext.getCurrentInstance().addMessage(null, msj);
	}
	
	public void removeUS(Usuarios use) {
		try {
			use.setPersonas(personas);
			use.setTipoU(tipousuario);
			daoUser.remove(use);
			mensaje = "Eliminado con �xito";
			init();
		}catch(Exception e) {
			e.printStackTrace();
		}
		FacesMessage msj = new FacesMessage(this.mensaje);
		FacesContext.getCurrentInstance().addMessage(null, msj);
	}
}



























//	public void getperson(Personas per) {
//		try {
//			personas = daousu.getperson(per);
//			System.out.println("me lo llevo:" + per.getId());
//			if (personas != null) {
//				System.out.println("encontrado");
//				FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("per", personas);
//			}
//			if (per.getId() != null) {
//				ExternalContext dc = FacesContext.getCurrentInstance().getExternalContext();
//				dc.redirect(dc.getRequestContextPath() + "/faces/CrearUsuario.xhtml");
//			}
//		} catch (Exception e) {
//			System.out.println("Error" + e.getMessage());
//			e.printStackTrace();
//		}
//	}
//