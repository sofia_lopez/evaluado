package com.agenda.ctrl;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.agenda.impl.ActividadesImpl;
import com.agenda.impl.EstadosImpl;
import com.agenda.model.Actividades;
import com.agenda.model.Estados;
import com.agenda.model.Fechasespeciales;

@Component
@RequestScoped
@ManagedBean
public class ActesDao implements Serializable {

	@Autowired
	private ActividadesImpl daoActi;

	@Autowired
	private EstadosImpl daoEst;

	private List<Actividades> listaActividades;
	private Actividades actividades;

	private List<Estados> listaEstados;
	private Estados estados;
	
	String mensaje = "";

	public List<Actividades> getListaActividades() {
		this.listaActividades = daoActi.findAll();
		return listaActividades;
	}

	public void setListaActividades(List<Actividades> listaActividades) {
		this.listaActividades = listaActividades;
	}

	public Actividades getActividades() {
		return actividades;
	}

	public void setActividades(Actividades actividades) {
		this.actividades = actividades;
	}

	public List<Estados> getListaEstados() {
		this.listaEstados = daoEst.findAll();
		return listaEstados;
	}

	public void setListaEstados(List<Estados> listaEstados) {
		this.listaEstados = listaEstados;
	}

	public Estados getEstados() {
		return estados;
	}

	public void setEstados(Estados estados) {
		this.estados = estados;
	}

	@PostConstruct
	public void init() {
		actividades = new Actividades();
		estados = new Estados();
	}

//	crud de actividades
	public void createActes() {
		try {
			actividades.setEstado(estados);
			daoActi.create(actividades);
			mensaje = "Registrado";
			init();
		}catch(Exception e) {
			e.printStackTrace();
		}
		FacesMessage msj = new FacesMessage(this.mensaje);
		FacesContext.getCurrentInstance().addMessage(null, msj);
	}
	
	public void editActes() {
		try {
			actividades.setEstado(estados);
			daoActi.create(actividades);
			mensaje = "Editado";
			init();
		}catch(Exception e) {
			e.printStackTrace();
		}
		FacesMessage msj = new FacesMessage(this.mensaje);
		FacesContext.getCurrentInstance().addMessage(null, msj);
	}
	
	public void deleteActes(Actividades es) {
		try {
			es.setEstado(estados);
			daoActi.remove(es);
			mensaje = "Eliminado con �xito";
		}catch(Exception e) {
			mensaje = "Error al eliminar";
			e.printStackTrace();
		}
		FacesMessage msj = new FacesMessage(this.mensaje);
		FacesContext.getCurrentInstance().addMessage(null, msj);
	}

	public void cargarActes(Actividades actes) {
		actividades.setEstado(estados);
		actividades = actes;
	}
	
	public String busqueda;

	public String getBusqueda() {
		return busqueda;
	}

	public void setBusqueda(String busqueda) {
		this.busqueda = busqueda;
	}
}
//	public void buscarUsuario(String comienza) {
//		try {
//			System.out.println("Entro");
//			System.out.println(comienza);
//			listaActividades = daoActi.consPSeguro(comienza);
//		} catch (Exception e) {
//			System.out.println("Error al realizar la busqueda de poliza, por: " + e.getMessage());
//		}
//	}
//}
