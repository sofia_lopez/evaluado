package com.agenda.ctrl;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.agenda.impl.FasesDetImpl;
import com.agenda.impl.FasesImpl;
import com.agenda.model.Fases;
import com.agenda.model.FasesDet;

@RequestScoped
@ManagedBean
@Component
public class FasesDao implements Serializable {
	
	@Autowired
	private FasesImpl daoFases;
	
	@Autowired
	private FasesDetImpl daoFasdet;
	
	private List<Fases> listaFases;
	private Fases fases;
	
	private List<FasesDet> listaFasdet;
	private FasesDet fasesDet;
	
	public List<Fases> getListaFases() {
		this.listaFases = daoFases.findAll();
		return listaFases;
	}
	public void setListaFases(List<Fases> listaFases) {
		this.listaFases = listaFases;
	}
	public Fases getFases() {
		return fases;
	}
	public void setFases(Fases fases) {
		this.fases = fases;
	}
	public List<FasesDet> getListaFasdet() {
		this.listaFases = daoFases.findAll();
		return listaFasdet;
	}
	public void setListaFasdet(List<FasesDet> listaFasdet) {
		this.listaFasdet = listaFasdet;
	}
	public FasesDet getFasesDet() {
		return fasesDet;
	}
	public void setFasesDet(FasesDet fasesDet) {
		this.fasesDet = fasesDet;
	}
	
	

}
