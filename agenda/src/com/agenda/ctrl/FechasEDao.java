package com.agenda.ctrl;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.agenda.impl.FechasEspecialesImpl;
import com.agenda.model.Actividades;
import com.agenda.model.Fechasespeciales;

@Component
@RequestScoped
@ManagedBean
public class FechasEDao implements Serializable {

	@Autowired
	private FechasEspecialesImpl daoFechas;

	private List<Fechasespeciales> listaFechas;
	private Fechasespeciales fechasespeciales;

	String mensaje = "";

	public List<Fechasespeciales> getListaFechas() {
		this.listaFechas = daoFechas.findAll();
		return listaFechas;
	}

	public void setListaFechas(List<Fechasespeciales> listaFechas) {
		this.listaFechas = listaFechas;
	}

	public Fechasespeciales getFechasespeciales() {
		return fechasespeciales;
	}

	public void setFechasespeciales(Fechasespeciales fechasespeciales) {
		this.fechasespeciales = fechasespeciales;
	}

	@PostConstruct
	public void init() {
		fechasespeciales = new Fechasespeciales();
	}

	public void create() {
		try {
			daoFechas.create(fechasespeciales);
			mensaje = "Registrado";
			fechasespeciales = new Fechasespeciales();
		} catch (Exception e) {
			e.printStackTrace();
		}
		FacesMessage msj = new FacesMessage(this.mensaje);
		FacesContext.getCurrentInstance().addMessage(null, msj);
	}

	public void edit() {
		try {
			daoFechas.edit(fechasespeciales);
			mensaje = "Editado";
		} catch (Exception e) {
			e.printStackTrace();
		}
		FacesMessage msj = new FacesMessage(this.mensaje);
		FacesContext.getCurrentInstance().addMessage(null, msj);
	}

	public void delete(Fechasespeciales fec) {
		try {
			daoFechas.remove(fec);
			mensaje = "Eliminado con �xito";
		} catch (Exception e) {
			mensaje = "Error";
			e.printStackTrace();
		}
		FacesMessage msj = new FacesMessage(this.mensaje);
		FacesContext.getCurrentInstance().addMessage(null, msj);

	}
	
	public void cargarFechas(Fechasespeciales fec) {
		fechasespeciales.setId(fec.getId());
		fechasespeciales = fec;
	}


}
