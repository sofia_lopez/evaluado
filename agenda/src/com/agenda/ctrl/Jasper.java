package com.agenda.ctrl;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
//import com.seguros.util.Conexion;

import com.agenda.util.Conexion;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperRunManager;

@ManagedBean
@SessionScoped
@Component
public class Jasper {

	Conexion con = new Conexion();



	public void reportesPDF() throws JRException, IOException {

		try {
			File jasper = new File(
					FacesContext.getCurrentInstance().getExternalContext().getRealPath("resources/Jasper/Actividades.jasper"));
			Map<String, Object> parameter = new HashMap<String, Object>();
			parameter.put("count", "root");
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasper.getPath(), parameter, con.conectar());
			byte[] bytes = JasperRunManager.runReportToPdf(jasper.getPath(), parameter, con.conectar());
			HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext()
					.getResponse();
			response.setContentType("application/pdf");
			response.setContentLength(bytes.length);
			response.setHeader("Content-disposition", "inline; filename=Prueba.pdf");
			ServletOutputStream os = response.getOutputStream();
			os.write(bytes, 0, bytes.length);
			os.flush();
			os.close();
			System.out.println("Funciona");
			FacesContext.getCurrentInstance().responseComplete();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
