package com.agenda.model;
// Generated 12-13-2019 11:03:12 AM by Hibernate Tools 5.2.12.Final

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.ColumnTransformer;

/**
 * Usuarios generated by hbm2java
 */
@Entity
@Table(name = "usuarios", catalog = "agendaElectronica", uniqueConstraints = @UniqueConstraint(columnNames = "usuario"))
public class Usuarios implements java.io.Serializable {

	private int id;
	private String usuario;
	private String pass;
	private Personas personas;
	private Tipousuario tipoU;

	public Usuarios() {
	}

	public Usuarios(int id, String usuario) {
		this.id = id;
		this.usuario = usuario;
	}

	public Usuarios(int id, String usuario, String pass, Personas personas, Tipousuario tipoU) {
		this.id = id;
		this.usuario = usuario;
		this.pass = pass;
		this.personas = personas;
		this.tipoU = tipoU;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name = "usuario", unique = true, nullable = false, length = 50)
	public String getUsuario() {
		return this.usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	@Column(columnDefinition = "BLOB", name = "pass")
	@ColumnTransformer(read = "AES_ENCRYPT(pass, 'afe')", write = "AES_ENCRYPT(?,'afe')") 
	public String getPass() {
		return this.pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	@ManyToOne(fetch = FetchType.EAGER,optional = false)
	@JoinColumn(name = "personas")
	public Personas getPersonas() {
		return this.personas;
	}

	public void setPersonas(Personas personas) {
		this.personas = personas;
	}

	@ManyToOne(fetch = FetchType.EAGER,optional = false)
	@JoinColumn(name = "tipoU")
	public Tipousuario getTipoU() {
		return this.tipoU;
	}

	public void setTipoU(Tipousuario tipoU) {
		this.tipoU = tipoU;
	}

}
