package com.agenda.model;
// Generated 12-13-2019 11:03:12 AM by Hibernate Tools 5.2.12.Final

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Actividades generated by hbm2java
 */
@Entity
@Table(name = "actividades", catalog = "agendaElectronica")
public class Actividades implements java.io.Serializable {

	private int id;
	private String nombre;
	private String descripcion;
	private Date fechaI;
	private Estados estado;
	private Usuarios usuarios;

	public Actividades() {
	}

	public Actividades(int id) {
		this.id = id;
	}

	public Actividades(int id, String nombre, String descripcion, Date fechaI, Estados estado, Usuarios usuarios) {
		this.id = id;
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.fechaI = fechaI;
		this.estado = estado;
		this.usuarios = usuarios;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name = "nombre", length = 50)
	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Column(name = "descripcion", length = 65535)
	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "fechaI", length = 10)
	public Date getFechaI() {
		return this.fechaI;
	}

	public void setFechaI(Date fechaI) {
		this.fechaI = fechaI;
	}

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "estado")
	public Estados getEstado() {
		return this.estado;
	}

	public void setEstado(Estados estado) {
		this.estado = estado;
	}

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "usuarios")
	public Usuarios getUsuarios() {
		return this.usuarios;
	}

	public void setUsuarios(Usuarios usuarios) {
		this.usuarios = usuarios;
	}

}
