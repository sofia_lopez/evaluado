package com.agenda.model;
// Generated 12-13-2019 11:03:12 AM by Hibernate Tools 5.2.12.Final

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Estados generated by hbm2java
 */
@Entity
@Table(name = "estados", catalog = "agendaElectronica")
public class Estados implements java.io.Serializable {

	private int id;
	private Boolean estado;

	public Estados() {
	}

	public Estados(int id) {
		this.id = id;
	}

	public Estados(int id, Boolean estado) {
		this.id = id;
		this.estado = estado;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name = "estado")
	public Boolean getEstado() {
		return this.estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

}
