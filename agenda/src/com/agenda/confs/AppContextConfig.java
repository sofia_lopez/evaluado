package com.agenda.confs;

import java.util.Properties;

import javax.sql.DataSource;

import org.apache.tomcat.dbcp.dbcp2.BasicDataSource;
import org.hibernate.SessionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBuilder;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.agenda.impl.ActividadesImpl;
import com.agenda.impl.EstadosImpl;
import com.agenda.impl.FasesDetImpl;
import com.agenda.impl.FasesImpl;
import com.agenda.impl.FechasEspecialesImpl;
import com.agenda.impl.PersonasImpl;
import com.agenda.impl.TipoUsuarioImpl;
import com.agenda.impl.UsuariosImpl;

@Configuration
@ComponentScan(basePackages = { "com.agenda" })
@EnableTransactionManagement(proxyTargetClass = true)
public class AppContextConfig {

	@Bean(name = "dataSource")
	public DataSource getDataSource() {
		BasicDataSource dataSource = new BasicDataSource();
		dataSource.setDriverClassName("com.mysql.jdbc.Driver");
		dataSource.setUrl(
				"jdbc:mysql://localhost:3306/agendaElectronica?allowPublicKeyRetrieval=true&useSSL=false&sessionVariables=LC_TIME_NAMES='es_SV'");
		dataSource.setUsername("root");
		dataSource.setPassword("root");
		return dataSource;
	}

	private Properties getHibernateProperties() {
		Properties properties = new Properties();
		properties.put("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
		properties.put("hibernate.format_sql", "hibernate.format_sql");
		return properties;
	}

	@Bean(name = "sessionFactory")
	public SessionFactory getsessionFactory(DataSource dataSource) {
		LocalSessionFactoryBuilder sessionBuilder = new LocalSessionFactoryBuilder(dataSource);
		sessionBuilder.scanPackages("com.agenda.model");
		sessionBuilder.addProperties(getHibernateProperties());
		return sessionBuilder.buildSessionFactory();
	}

	@Bean(name = "transactionManager")
	public HibernateTransactionManager getTransactionManager(SessionFactory sessionFactory) {
		HibernateTransactionManager transactionManager = new HibernateTransactionManager(sessionFactory);
		return transactionManager;
	}

	@Bean
	public PersonasImpl personas() {
		return new PersonasImpl();
	}

	@Bean
	ActividadesImpl actividades() {
		return new ActividadesImpl();
	}

	@Bean
	EstadosImpl estados() {
		return new EstadosImpl();
	}

	@Bean
	FasesDetImpl fasesDet() {
		return new FasesDetImpl();
	}

	@Bean
	FasesImpl fases() {
		return new FasesImpl();
	}

	@Bean
	FechasEspecialesImpl fechasEspeciales() {
		return new FechasEspecialesImpl();
	}

	@Bean
	TipoUsuarioImpl tipoUsuario() {
		return new TipoUsuarioImpl();
	}

	@Bean
	UsuariosImpl usuarios() {
		return new UsuariosImpl();
	}
}
