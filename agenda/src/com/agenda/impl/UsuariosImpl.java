package com.agenda.impl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.agenda.model.Usuarios;
import com.agenda.util.AbstractFacade;
import com.agenda.util.Dao;

@Repository("usuarios")
public class UsuariosImpl extends AbstractFacade<Usuarios> implements Dao<Usuarios> {

	@Autowired
	private SessionFactory sessionFactory;

	public UsuariosImpl() {
		super(Usuarios.class);
	}

	@Override
	protected SessionFactory sessionFactory() {
		return sessionFactory;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Transactional
	public Usuarios access(String usuario, String pass) {

		Usuarios usu = null;
		try {
			String consulta = "SELECT u.id FROM Usuarios as u WHERE u.usuario=? and u.pass =aes_encrypt(?,'afe')";
			Query q = sessionFactory.getCurrentSession().createNativeQuery(consulta);
			q.setParameter(1, usuario);
			q.setParameter(2, pass);
			System.out.println("Se ejecuto");
			List<Usuarios> listaUser = q.getResultList();

			if (listaUser.size() > 0) {
				String sql2 = "SELECT u FROM Usuarios u WHERE u.usuario=?1";
				Query q2 = sessionFactory.getCurrentSession().createQuery(sql2);
				q2.setParameter(1, usuario);
				List<Usuarios> list2 = q2.getResultList();
				usu = list2.get(0);
				System.out.println("Usuario" + usu.getUsuario());
			}
		} catch (Exception e) {
			System.out.println("Error" + e.getMessage());
			e.printStackTrace();
		}
		return usu;
	}
}
