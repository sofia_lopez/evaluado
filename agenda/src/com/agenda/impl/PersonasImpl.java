package com.agenda.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.agenda.model.Personas;
import com.agenda.util.AbstractFacade;
import com.agenda.util.Dao;

@Repository("personas")
public class PersonasImpl extends AbstractFacade<Personas> implements Dao<Personas> {

	@Autowired
	private SessionFactory sessionFactory;

	public PersonasImpl() {
		super(Personas.class);
	}

	@Override
	protected SessionFactory sessionFactory() {
		return sessionFactory;
	}
}
