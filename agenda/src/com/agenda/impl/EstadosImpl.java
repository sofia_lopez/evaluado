package com.agenda.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.agenda.model.Estados;
import com.agenda.util.AbstractFacade;
import com.agenda.util.Dao;

@Repository("estados")
public class EstadosImpl extends AbstractFacade<Estados> implements Dao<Estados> {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public EstadosImpl() {
		super(Estados.class);
	}
	
	@Override
	protected SessionFactory sessionFactory() {
		return sessionFactory;
	}

}
