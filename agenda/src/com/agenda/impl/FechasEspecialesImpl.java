package com.agenda.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.agenda.model.Fechasespeciales;
import com.agenda.util.AbstractFacade;
import com.agenda.util.Dao;

@Repository("fechasEspeciales")
public class FechasEspecialesImpl extends AbstractFacade<Fechasespeciales> implements Dao<Fechasespeciales> {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public FechasEspecialesImpl() {
		super(Fechasespeciales.class);
	}
	
	@Override
	protected SessionFactory sessionFactory() {
		return sessionFactory;
	}

}
