package com.agenda.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.agenda.model.Fases;
import com.agenda.util.AbstractFacade;
import com.agenda.util.Dao;

@Repository("fases")
public class FasesImpl extends AbstractFacade<Fases> implements Dao<Fases> {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public FasesImpl() {
		super(Fases.class);
	}
	
	@Override
	protected SessionFactory sessionFactory() {
		return sessionFactory;
	}

}
