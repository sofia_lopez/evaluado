package com.agenda.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.agenda.model.FasesDet;
import com.agenda.util.AbstractFacade;
import com.agenda.util.Dao;

@Repository("fasesDet")
public class FasesDetImpl extends AbstractFacade<FasesDet> implements Dao<FasesDet> {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public FasesDetImpl() {
		super(FasesDet.class);
	}
	
	@Override
	protected SessionFactory sessionFactory() {
		return sessionFactory;
	}

}
