package com.agenda.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.agenda.model.Tipousuario;
import com.agenda.util.AbstractFacade;
import com.agenda.util.Dao;

@Repository("tipoUsuario")
public class TipoUsuarioImpl extends AbstractFacade<Tipousuario> implements Dao<Tipousuario> {

	@Autowired
	private SessionFactory sessionFactory;

	public TipoUsuarioImpl() {
		super(Tipousuario.class);
	}

	@Override
	protected SessionFactory sessionFactory() {
		return sessionFactory;
	}

}
