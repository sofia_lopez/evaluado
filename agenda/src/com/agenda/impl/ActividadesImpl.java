package com.agenda.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.agenda.model.Actividades;
import com.agenda.model.Usuarios;
import com.agenda.util.AbstractFacade;
import com.agenda.util.Dao;

@Repository("actividades")
public class ActividadesImpl extends AbstractFacade<Actividades> implements Dao<Actividades> {

	@Autowired
	private SessionFactory sessionFactory;

	public ActividadesImpl() {
		super(Actividades.class);
	}

	@Override
	protected SessionFactory sessionFactory() {
		return sessionFactory;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public List<Actividades> busquedaxUsuario(String user) {
		List<Actividades> listxUsuario = new ArrayList<>();
		try {
			CriteriaBuilder cb = sessionFactory.getCriteriaBuilder();
			CriteriaQuery<Actividades> cq = cb.createQuery(Actividades.class);
			Root<Actividades> rootActividades = cq.from(Actividades.class);
			Join<Actividades, Usuarios> join01 = rootActividades.join("Usuarios");
			cq.select(rootActividades);
			cq.where(cb.like(join01.get("user"), user));
			Query q = sessionFactory.getCurrentSession().createQuery(cq);
			listxUsuario = q.getResultList();
			return listxUsuario;

		} catch (Exception e) {
			System.out.println("Error" + e.getMessage());
		}
		return null;
	}

}
